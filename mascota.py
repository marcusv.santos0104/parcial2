import random


class Mascota:
    codigo = 0
    alias = ""
    especie = ""
    raza = ""
    colorPelo = ""
    fechaNacimiento = ""
    pesoMedio = ""
    pesoActual = ""

    def __init__(self):
        self.codigo = random.randint(1, 10)
        print("--- Creando Mascota ---")

    def crearMascota(self, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual):
        self.alias = alias
        self.especie = especie
        self.raza = raza
        self.colorPelo = colorPelo
        self.fechaNacimiento = fechaNacimiento
        self.pesoMedio = pesoMedio
        self.pesoActual = pesoActual
        print("Creado con exito.")

    def actualizarMascota(self):
        self.codigo = int(input("Código: "))
        self.alias = input("Alias: ")
        self.especie = input("Especie: ")
        self.raza = input("Raza: ")
        self.colorPelo = input("Color del pelo: ")
        self.fechaNacimiento = input("Fecha de nacimiento: ")
        self.pesoMedio = float(input("Peso medio: "))
        self.pesoActual = float(input("Peso actual: "))
        print("Actualizado con exito.")

    def mostrarMascota(self):
        print("Código", "Alias", "Especie", "Raza", "Color del pelo", "Fecha de nacimiento", "Peso medio", "Peso actual", sep="\t")
        print(self.codigo, self.alias, self.especie, self.raza, self.colorPelo, self.fechaNacimiento, self.pesoMedio, self.pesoActual, sep="\t")

    def borrarMascota(self):
        self.codigo = 0
        self.alias = ""
        self.especie = ""
        self.raza = ""
        self.colorPelo = ""
        self.fechaNacimiento = ""
        self.pesoMedio = ""
        self.pesoActual = ""
        print("Actualizado con exito.")


class HistorialMedico(Mascota):
    enfermedad = ""
    fechaEnfermo = ""

    def crearHistorialMedico(self, enfermedad, fechaEnfermo):
        self.enfermedad = enfermedad
        self.fechaEnfermo = fechaEnfermo
        print("Creado con exito.")

    def actualizarHistorialMedico(self):
        self.enfermedad = input("Enfermedad: ")
        self.fechaEnfermo = input("Fecha enfermo: ")

    def mostrarHistorialMedico(self):
        print("Código", "Alias", "Especie", "Raza", "Color del pelo", "Fecha de nacimiento", "Peso medio", "Peso actual", "Enfermedad", "Fecha enfermo", sep="\t")
        print(self.codigo, self.alias, self.especie, self.raza, self.colorPelo, self.fechaNacimiento, self.pesoMedio, self.pesoActual, self.enfermedad, self.fechaEnfermo, sep="\t")

    def borrarHistorialMedico(self):
        self.enfermedad = ""
        self.fechaEnfermo = ""
        print("Actualizado con exito.")


class CalendarioDeVacunacion(HistorialMedico):
    fechaCadaVacuna = ""
    enfermedadQueSeVacuno = ""

    def crearCalendarioDeVacunacion(self, fechaCadaVacuna, enfermedadQueSeVacuno):
        self.fechaCadaVacuna = fechaCadaVacuna
        self.enfermedadQueSeVacuno = enfermedadQueSeVacuno
        print("Creado con exito.")

    def actualizarCalendarioDeVacunacion(self):
        self.fechaCadaVacuna = input("Fecha de cada vacuna: ")
        self.enfermedadQueSeVacuno = input("Enfermedad que se vacunó: ")

    def mostrarCalendarioDeVacunacion(self):
        print("Codigo del animal", "Fecha de cada vacuna", "Enfermedad que se vacuno", sep="\t")
        print(self.codigo, self.fechaCadaVacuna, self.enfermedadQueSeVacuno, sep="\t")

    def borrarCalendarioDeVacunacion(self):
        self.fechaCadaVacuna = ""
        self.enfermedadQueSeVacuno = ""
        print("Actualizado con exito.")
