from cliente import *
from mascota import *

if __name__ == '__main__':
    print("--- Veterinaria ---")
    print("1. Clientes\n2. Mascotas\n3. Historial médico\n4. Calendário de Vacunación\n5. Salir")
    opcion = int(input())
    while opcion != 5:
        # Clientes
        if opcion == 1:
            while opcion != 5:
                print("--- Clientes ---\n1. Crear\n2. Actualizar\n3. Mostrar\n4. Borrar\n5. Salir")
                opcion = int(input())
                if opcion == 1:
                    cedula = input("Cedula: ")
                    codigoDeCliente = int(input("Codigo de cliente: "))
                    primerApellido = input("Primer apellido: ")
                    nombres = input("Nombres: ")
                    direccion = input("Dirección: ")
                    telefono = input("Teléfono: ")
                    cliente = Cliente(cedula, codigoDeCliente, primerApellido, nombres, direccion, telefono)
                    print("Creado con exito.")
                elif opcion == 2:
                    cliente.actualizarCliente()
                elif opcion == 3:
                    cliente.mostrarCliente()
                elif opcion == 4:
                    cliente.borrarCliente()

        # Mascotas
        elif opcion == 2:
            while opcion != 5:
                print("--- Mascotas ---\n1. Crear\n2. Actualizar\n3. Mostrar\n4. Borrar\n5. Salir")
                opcion = int(input())
                if opcion == 1:
                    mascota = Mascota()
                    alias = input("Alias: ")
                    especie = input("Especie: ")
                    raza = input("Raza: ")
                    colorPelo = input("Color del pelo: ")
                    fechaNacimiento = input("Fecha de nacimiento: ")
                    pesoMedio = input("Peso medio: ")
                    pesoActual = input("Peso actual: ")
                    mascota.crearMascota(alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)
                elif opcion == 2:
                    mascota.actualizarMascota()
                elif opcion == 3:
                    mascota.mostrarMascota()
                elif opcion == 4:
                    mascota.borrarMascota()

        # Historial médico
        elif opcion == 3:
            while opcion != 5:
                print("--- Historial Médico ---\n1. Crear\n2. Actualizar\n3. Mostrar\n4. Borrar\n5. Salir")
                opcion = int(input())
                if opcion == 1:
                    historial = HistorialMedico()
                    alias = input("Alias: ")
                    especie = input("Especie: ")
                    raza = input("Raza: ")
                    colorPelo = input("Color del pelo: ")
                    fechaNacimiento = input("Fecha de nacimiento: ")
                    pesoMedio = input("Peso medio: ")
                    pesoActual = input("Peso actual: ")
                    historial.crearMascota(alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)
                    enfermedad = input("Enfermedad: ")
                    fechaEnfermo = input("Fecha enfermo: ")
                    historial.crearHistorialMedico(enfermedad, fechaEnfermo)
                elif opcion == 2:
                    historial.actualizarHistorialMedico()
                elif opcion == 3:
                    historial.mostrarHistorialMedico()
                elif opcion == 4:
                    historial.borrarHistorialMedico()

        # Calendario de Vacunacion
        elif opcion == 4:
            while opcion != 5:
                print("--- Calendario de Vacunacion ---\n1. Crear\n2. Actualizar\n3. Mostrar\n4. Borrar\n5. Salir")
                opcion = int(input())
                if opcion == 1:
                    calendario = CalendarioDeVacunacion()
                    alias = input("Alias: ")
                    especie = input("Especie: ")
                    raza = input("Raza: ")
                    colorPelo = input("Color del pelo: ")
                    fechaNacimiento = input("Fecha de nacimiento: ")
                    pesoMedio = input("Peso medio: ")
                    pesoActual = input("Peso actual: ")
                    calendario.crearMascota(alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)
                    enfermedad = input("Enfermedad: ")
                    fechaEnfermo = input("Fecha enfermo: ")
                    calendario.crearHistorialMedico(enfermedad, fechaEnfermo)
                    fechaCadaVacuna = input("Fecha cada vacuna: ")
                    enfermedadQueSeVacuno = input("Enfermedad que se vacuno: ")
                    calendario.crearCalendarioDeVacunacion(fechaCadaVacuna, enfermedadQueSeVacuno)
                elif opcion == 2:
                    calendario.actualizarCalendarioDeVacunacion()
                elif opcion == 3:
                    calendario.mostrarCalendarioDeVacunacion()
                elif opcion == 4:
                    calendario.borrarCalendarioDeVacunacion()
