class Cliente:
    def __init__(self, cedula, codigo_cliente, primer_apellido, nombres, direccion, telefono):
        self.cedula = cedula
        self.codigoDeCliente = codigo_cliente
        self.primerApellido = primer_apellido
        self.nombres = nombres
        self.direccion = direccion
        self.telefono = telefono

    def actualizarCliente(self):
        self.cedula = input("Cedula: ")
        self.codigoDeCliente = int(input("Codigo de cliente: "))
        self.primerApellido = input("Primer apellido: ")
        self.nombres = input("Nombres: ")
        self.direccion = input("Dirección: ")
        self.telefono = input("Teléfono: ")
        print("Actualizado con exito.")

    def mostrarCliente(self):
        print("Cedula", "Codigo de cliente", "Primer apellido", "Nombres", "Dirección", "Teléfono", sep="\t")
        print(self.cedula, self.codigoDeCliente, self.primerApellido, self.nombres, self.direccion, self.telefono, sep="\t")

    def borrarCliente(self):
        self.cedula = ""
        self.codigoDeCliente = ""
        self.primerApellido = ""
        self.nombres = ""
        self.direccion = ""
        self.telefono = ""
        print("Borrado con exito.")
